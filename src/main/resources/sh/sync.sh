
#!/bin/bash

JAR_FILE="/root/xxxx/data_copy/SyncPg-0.0.1-SNAPSHOT.jar"
LOG_DIR="$(pwd)/logs"
LOG_FILE="${LOG_DIR}/$(date +'%Y-%m-%d').log"
MAX_LOG_FILES=7

function start() {
    if pgrep -f $JAR_FILE >/dev/null; then
        echo "The process is already running."
    else
        nohup java -jar $JAR_FILE >> $LOG_FILE 2>&1 &
        echo "Process started. Log file: $LOG_FILE"
    fi
}

function stop() {
    if pgrep -f $JAR_FILE >/dev/null; then
        pkill -f $JAR_FILE
        echo "Process stopped."
    else
        echo "The process is not running."
    fi
}

function restart() {
    stop
    start
}

function cleanup_logs() {
    local num_logs=$(ls -1 $LOG_DIR | wc -l)
    if [ $num_logs -gt $MAX_LOG_FILES ]; then
        local num_to_delete=$((num_logs - MAX_LOG_FILES))
        for i in $(ls -1t $LOG_DIR | tail -n $num_to_delete); do
            rm -f "$LOG_DIR/$i"
        done
        echo "$num_to_delete log file(s) deleted."
    else
        echo "No log files need to be deleted."
    fi
}

# 创建日志目录（如果不存在）
mkdir -p $LOG_DIR

# 根据参数执行相应操作
case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        restart
        ;;
    cleanup)
        cleanup_logs
        ;;
    *)
        echo "Usage: $0 {start|stop|restart|cleanup}"
        exit 1
        ;;
esac
