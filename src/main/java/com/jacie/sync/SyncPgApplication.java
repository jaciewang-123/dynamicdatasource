package com.jacie.sync;

import com.jacie.sync.config.OceanbaseDataSourceConfig;
import com.jacie.sync.config.PgDataSourceConfig;
import com.jacie.sync.config.TaosDataSourceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({OceanbaseDataSourceConfig.class, PgDataSourceConfig.class, TaosDataSourceConfig.class})
public class SyncPgApplication {

    public static void main(String[] args) {
        SpringApplication.run(SyncPgApplication.class, args);
    }

}
