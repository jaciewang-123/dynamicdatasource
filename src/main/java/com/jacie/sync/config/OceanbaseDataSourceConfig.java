package com.jacie.sync.config;


import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;

@Configuration
@MapperScan(basePackages = "com.jacie.sync.mapper.oceanbase", sqlSessionTemplateRef = "oceanbaseSqlSessionTemplate")
public class OceanbaseDataSourceConfig {
    @Value("${spring.datasource.druid.oceanbase.url}")
    private String url;

    @Value("${spring.datasource.druid.oceanbase.username}")
    private String username;

    @Value("${spring.datasource.druid.oceanbase.password}")
    private String password;

    @Value("${spring.datasource.druid.oceanbase.driverClassName}")
    private String driverClassName;

    @Bean(name = "oceanbaseDataSource")
    public DataSource oceanbaseDataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setDriverClassName(driverClassName);
        // 其他数据源配置
        return dataSource;
    }

    @Bean(name = "oceanbaseSqlSessionFactory")
    public SqlSessionFactory oceanbaseSqlSessionFactory(@Qualifier("oceanbaseDataSource") DataSource dataSource) throws Exception {
        MybatisSqlSessionFactoryBean sessionFactoryBean = new MybatisSqlSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource);
        sessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:mapper/oceanbase/**/*.xml"));
        // 其他配置
        return sessionFactoryBean.getObject();
    }

    @Bean(name = "oceanbaseSqlSessionTemplate")
    public SqlSessionTemplate oceanbaseSqlSessionTemplate(@Qualifier("oceanbaseSqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}