package com.jacie.sync.mapper.oceanbase;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.oceanbase.SnptMrv;

/**
* @author lzg
* @description 针对表【snpt_mrv(MRV报告)】的数据库操作Mapper
* @createDate 2023-07-05 15:11:01
* @Entity com.jacie.base.energy.domain.SnptMrv
*/
public interface SnptMrvMapper extends BaseMapper<SnptMrv> {

}




