package com.jacie.sync.mapper.oceanbase;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.oceanbase.RptDaily;

import java.util.List;

/**
 * 每日报告Mapper接口
 * 
 * @author jacie
 * @date 2023-09-21
 */
public interface RptDailyMapper extends BaseMapper<RptDaily>
{
    /**
     * 查询每日报告
     * 
     * @param rptDailyId 每日报告主键
     * @return 每日报告
     */
    public RptDaily selectRptDailyByRptDailyId(Long rptDailyId);

    /**
     * 查询每日报告列表
     * 
     * @param rptDaily 每日报告
     * @return 每日报告集合
     */
    public List<RptDaily> selectRptDailyList(RptDaily rptDaily);

    /**
     * 新增每日报告
     * 
     * @param rptDaily 每日报告
     * @return 结果
     */
    public int insertRptDaily(RptDaily rptDaily);

    /**
     * 修改每日报告
     * 
     * @param rptDaily 每日报告
     * @return 结果
     */
    public int updateRptDaily(RptDaily rptDaily);

    /**
     * 删除每日报告
     * 
     * @param rptDailyId 每日报告主键
     * @return 结果
     */
    public int deleteRptDailyByRptDailyId(Long rptDailyId);

    /**
     * 批量删除每日报告
     * 
     * @param rptDailyIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRptDailyByRptDailyIds(Long[] rptDailyIds);
}
