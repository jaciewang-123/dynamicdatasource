package com.jacie.sync.mapper.oceanbase;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.oceanbase.RptEnergyIndex;

import java.util.List;

/**
 * 能效指标数据报告Mapper接口
 * 
 * @author jacie
 * @date 2023-09-21
 */
public interface RptEnergyIndexMapper extends BaseMapper<RptEnergyIndex>
{
    /**
     * 查询能效指标数据报告
     * 
     * @param rptEnergyId 能效指标数据报告主键
     * @return 能效指标数据报告
     */
    public RptEnergyIndex selectRptEnergyIndexByRptEnergyId(Long rptEnergyId);

    /**
     * 查询能效指标数据报告列表
     * 
     * @param rptEnergyIndex 能效指标数据报告
     * @return 能效指标数据报告集合
     */
    public List<RptEnergyIndex> selectRptEnergyIndexList(RptEnergyIndex rptEnergyIndex);

    /**
     * 新增能效指标数据报告
     * 
     * @param rptEnergyIndex 能效指标数据报告
     * @return 结果
     */
    public int insertRptEnergyIndex(RptEnergyIndex rptEnergyIndex);

    /**
     * 修改能效指标数据报告
     * 
     * @param rptEnergyIndex 能效指标数据报告
     * @return 结果
     */
    public int updateRptEnergyIndex(RptEnergyIndex rptEnergyIndex);

    /**
     * 删除能效指标数据报告
     * 
     * @param rptEnergyId 能效指标数据报告主键
     * @return 结果
     */
    public int deleteRptEnergyIndexByRptEnergyId(Long rptEnergyId);

    /**
     * 批量删除能效指标数据报告
     * 
     * @param rptEnergyIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRptEnergyIndexByRptEnergyIds(Long[] rptEnergyIds);
}
