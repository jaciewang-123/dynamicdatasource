package com.jacie.sync.mapper.oceanbase;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.oceanbase.SnptDaily;
import org.apache.ibatis.annotations.Param;

/**
* @author lzg
* @description 针对表【snpt_daily(每天统计计算值表(每天快照))】的数据库操作Mapper
* @createDate 2023-05-09 14:35:28
* @Entity com.jacie.base.energy.domain.SnptDaily
*/
public interface SnptDailyMapper extends BaseMapper<SnptDaily> {

    /**
     * 查询所有可求均值的数据
     * @param shipId
     * @param startTime
     * @param endTime
     * @param navigaStatus 航行状态
     * @return
     */
    SnptDaily avgData(@Param("shipId") Long shipId,
                      @Param("startTime") String startTime,
                      @Param("endTime") String endTime,
                      @Param("navigaStatus") String navigaStatus);


    /**
     * 查询某列之和
     * @param shipId 船id
     * @param columnName 列名
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    Double sumColumnData(@Param("shipId") Long shipId,
                         @Param("columnName") String columnName,
                         @Param("startTime") String startTime,
                         @Param("endTime") String endTime);

    /**
     * 查询某列平均值
     * @param shipId 船id
     * @param columnName 列名
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    Double avgColumnData(@Param("shipId") Long shipId,
                         @Param("columnName") String columnName,
                         @Param("startTime") String startTime,
                         @Param("endTime") String endTime);


    /**
     * 查询某列xx值
     * @param shipId 船id
     * @param funcName 函数名称(sum,max,min.avg 等等)
     * @param columnName 列名
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    Double customColumnData(@Param("shipId") Long shipId,
                            @Param("funcName") String funcName,
                            @Param("columnName") String columnName,
                            @Param("startTime") String startTime,
                            @Param("endTime") String endTime);

    /**
     * 查询时间内最多的航行状态
     * @param shipId
     * @param startTime
     * @param endTime
     * @return
     */
    String maxNavigationStatus(@Param("shipId") Long shipId,
                               @Param("startTime") String startTime,
                               @Param("endTime") String endTime);
}




