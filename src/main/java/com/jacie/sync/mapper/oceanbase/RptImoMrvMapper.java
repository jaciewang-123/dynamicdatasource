package com.jacie.sync.mapper.oceanbase;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.oceanbase.RptImoMrv;

import java.util.List;

/**
 * MRV-IMOMapper接口
 * 
 * @author jacie
 * @date 2023-09-21
 */
public interface RptImoMrvMapper  extends BaseMapper<RptImoMrv>
{
    /**
     * 查询MRV-IMO
     * 
     * @param rptImoId MRV-IMO主键
     * @return MRV-IMO
     */
    public RptImoMrv selectRptImoMrvByRptImoId(Long rptImoId);

    /**
     * 查询MRV-IMO列表
     * 
     * @param rptImoMrv MRV-IMO
     * @return MRV-IMO集合
     */
    public List<RptImoMrv> selectRptImoMrvList(RptImoMrv rptImoMrv);

    /**
     * 新增MRV-IMO
     * 
     * @param rptImoMrv MRV-IMO
     * @return 结果
     */
    public int insertRptImoMrv(RptImoMrv rptImoMrv);

    /**
     * 修改MRV-IMO
     * 
     * @param rptImoMrv MRV-IMO
     * @return 结果
     */
    public int updateRptImoMrv(RptImoMrv rptImoMrv);

    /**
     * 删除MRV-IMO
     * 
     * @param rptImoId MRV-IMO主键
     * @return 结果
     */
    public int deleteRptImoMrvByRptImoId(Long rptImoId);

    /**
     * 批量删除MRV-IMO
     * 
     * @param rptImoIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRptImoMrvByRptImoIds(Long[] rptImoIds);
}
