package com.jacie.sync.mapper.oceanbase;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.oceanbase.RptEnergySynth;

import java.util.List;

/**
 * 能效能耗综合报告Mapper接口
 * 
 * @author jacie
 * @date 2023-09-21
 */
public interface RptEnergySynthMapper  extends BaseMapper<RptEnergySynth>
{
    /**
     * 查询能效能耗综合报告
     * 
     * @param id 能效能耗综合报告主键
     * @return 能效能耗综合报告
     */
    public RptEnergySynth selectRptEnergySynthById(Long id);

    /**
     * 查询能效能耗综合报告列表
     * 
     * @param rptEnergySynth 能效能耗综合报告
     * @return 能效能耗综合报告集合
     */
    public List<RptEnergySynth> selectRptEnergySynthList(RptEnergySynth rptEnergySynth);

    /**
     * 新增能效能耗综合报告
     * 
     * @param rptEnergySynth 能效能耗综合报告
     * @return 结果
     */
    public int insertRptEnergySynth(RptEnergySynth rptEnergySynth);

    /**
     * 修改能效能耗综合报告
     * 
     * @param rptEnergySynth 能效能耗综合报告
     * @return 结果
     */
    public int updateRptEnergySynth(RptEnergySynth rptEnergySynth);

    /**
     * 删除能效能耗综合报告
     * 
     * @param id 能效能耗综合报告主键
     * @return 结果
     */
    public int deleteRptEnergySynthById(Long id);

    /**
     * 批量删除能效能耗综合报告
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRptEnergySynthByIds(Long[] ids);

    RptEnergySynth selectLastByShipId(Long shipId);
}
