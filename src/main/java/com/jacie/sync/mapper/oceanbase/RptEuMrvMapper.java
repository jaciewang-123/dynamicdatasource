package com.jacie.sync.mapper.oceanbase;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.oceanbase.RptEuMrv;

import java.util.List;

/**
 * MRV-EUMapper接口
 * 
 * @author jacie
 * @date 2023-09-21
 */
public interface RptEuMrvMapper  extends BaseMapper<RptEuMrv>
{
    /**
     * 查询MRV-EU
     * 
     * @param rptEuId MRV-EU主键
     * @return MRV-EU
     */
    public RptEuMrv selectRptEuMrvByRptEuId(Long rptEuId);

    /**
     * 查询MRV-EU列表
     * 
     * @param rptEuMrv MRV-EU
     * @return MRV-EU集合
     */
    public List<RptEuMrv> selectRptEuMrvList(RptEuMrv rptEuMrv);

    /**
     * 新增MRV-EU
     * 
     * @param rptEuMrv MRV-EU
     * @return 结果
     */
    public int insertRptEuMrv(RptEuMrv rptEuMrv);

    /**
     * 修改MRV-EU
     * 
     * @param rptEuMrv MRV-EU
     * @return 结果
     */
    public int updateRptEuMrv(RptEuMrv rptEuMrv);

    /**
     * 删除MRV-EU
     * 
     * @param rptEuId MRV-EU主键
     * @return 结果
     */
    public int deleteRptEuMrvByRptEuId(Long rptEuId);

    /**
     * 批量删除MRV-EU
     * 
     * @param rptEuIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRptEuMrvByRptEuIds(Long[] rptEuIds);
}
