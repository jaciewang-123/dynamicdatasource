package com.jacie.sync.mapper.oceanbase;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.oceanbase.SnptTankDaily;

/**
* @author lzg
* @description 针对表【snpt_tank_daily(每天油舱余量记录)】的数据库操作Mapper
* @createDate 2023-05-22 14:38:57
* @Entity com.jacie.base.energy.domain.SnptTankDaily
*/
public interface SnptTankDailyMapper extends BaseMapper<SnptTankDaily> {

    SnptTankDaily sumAllData(Long shipId);
}




