package com.jacie.sync.mapper.oceanbase;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.oceanbase.SnptMonth;
import org.apache.ibatis.annotations.Param;

/**
* @author lzg
* @description 针对表【snpt_month(每月统计计算值表(每月快照))】的数据库操作Mapper
* @createDate 2023-07-12 09:53:51
* @Entity com.jacie.base.energy.domain.SnptMonth
*/
public interface SnptMonthMapper extends BaseMapper<SnptMonth> {

    /**
     * 查询某列之和
     * @param shipId 船id
     * @param columnName 列名
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    Double sumColumnData(@Param("shipId") Long shipId,
                         @Param("columnName") String columnName,
                         @Param("startTime") String startTime,
                         @Param("endTime") String endTime);
}




