package com.jacie.sync.mapper.oceanbase;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.oceanbase.SnptHour;
import org.apache.ibatis.annotations.Param;

/**
* @author lzg
* @description 针对表【snpt_hour(每小时统计计算值表)】的数据库操作Mapper
* @createDate 2023-05-09 14:35:33
* @Entity com.jacie.base.energy.domain.SnptHour
*/
public interface SnptHourMapper extends BaseMapper<SnptHour> {

    /**
     * 查询所有可计算数据每列之和
     * @param shipId 船id
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    SnptHour sumData(@Param("shipId") Long shipId,
                     @Param("startTime") String startTime,
                     @Param("endTime") String endTime);


    /**
     * 查询时间范围的最后新的一条数据
     * @param shipId 船舶id
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    SnptHour lastData(@Param("shipId") Long shipId,
                      @Param("startTime") String startTime,
                      @Param("endTime") String endTime);

    /**
     * 查询所有可求均值的数据
     * @param shipId
     * @param startTime
     * @param endTime
     * @return
     */
    SnptHour avgData(@Param("shipId") Long shipId,
                     @Param("startTime") String startTime,
                     @Param("endTime") String endTime,
                     @Param("navigaStatus") String navigaStatus);

    /**
     * 查询某列之和
     * @param shipId 船id
     * @param columnName 列名
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    Double sumColumnData(@Param("shipId") Long shipId,
                         @Param("columnName") String columnName,
                         @Param("startTime") String startTime,
                         @Param("endTime") String endTime);

    /**
     * 查询某列平均值
     * @param shipId 船id
     * @param columnName 列名
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    Double avgColumnData(@Param("shipId") Long shipId,
                         @Param("columnName") String columnName,
                         @Param("startTime") String startTime,
                         @Param("endTime") String endTime);


    /**
     * 查询某列xx值
     * @param shipId 船id
     * @param funcName 函数名称(sum,max,min.avg 等等)
     * @param columnName 列名
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    Double customColumnData(@Param("shipId") Long shipId,
                            @Param("funcName") String funcName,
                         @Param("columnName") String columnName,
                         @Param("startTime") String startTime,
                         @Param("endTime") String endTime);

    /**
     * 查询时间内最多的航行状态
     * @param shipId
     * @param startTime
     * @param endTime
     * @return
     */
    String maxNavigationStatus(@Param("shipId") Long shipId,
                               @Param("startTime") String startTime,
                               @Param("endTime") String endTime);

}




