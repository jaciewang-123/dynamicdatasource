package com.jacie.sync.mapper.taos;


import com.jacie.sync.domain.taos.AuxiliaryPm;
import com.jacie.sync.domain.taos.Boiler;
import com.jacie.sync.domain.taos.MainPm;
import com.jacie.sync.domain.taos.Panel;

/**
 * @author lzg
 * @description 添加
 * @date 2023/4/19 15:59
 */
public interface TaosInsertMapper {


     int addMainPm(MainPm mainPm);

     int addAuxiliaryPm(AuxiliaryPm auxiliaryPm);

     int addBoiler(Boiler boiler);

     int addPanel(Panel panel);


}
