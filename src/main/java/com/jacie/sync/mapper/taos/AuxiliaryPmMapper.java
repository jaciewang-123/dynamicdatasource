package com.jacie.sync.mapper.taos;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.taos.AuxiliaryPm;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lzg
 * @description 辅机
 * @date 2023/4/11 15:45
 */
public interface AuxiliaryPmMapper extends BaseMapper<AuxiliaryPm> {

    /**
     * 查询最新一条数据
     * @param shipCode
     * @param uniqueIndex
     */
    public List<AuxiliaryPm> selectNewData(@Param("shipCode") String shipCode, @Param("uniqueIndex") String uniqueIndex);

    /**
     * 查询累计油耗
     * @param shipCode
     * @param uniqueIndex
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    public List<AuxiliaryPm> selectSumFlow(@Param("shipCode") String shipCode,
                                           @Param("uniqueIndex") String uniqueIndex,
                                           @Param("startTime") String startTime,
                                           @Param("endTime") String endTime);

}
