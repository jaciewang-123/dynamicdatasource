package com.jacie.sync.mapper.taos;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.taos.Sb;

/**
 * @author jacie.wang@outlook.com
 * @description 配电板数据
 * @date 2023/4/11 15:45
 */
public interface SbMapper extends BaseMapper<Sb> {

    void addSb(Sb sb);

}
