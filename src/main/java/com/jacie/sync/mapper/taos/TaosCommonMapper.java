package com.jacie.sync.mapper.taos;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lzg
 * @description taos公共mapper
 * @date 2023/4/13 16:53
 */
public interface TaosCommonMapper {

    /**
     * 计算差值(最大和最小值)
     * 注意此方法只适合数值一直自增的数据
     *
     * @param shipCode
     * @param uniqueIndex
     * @param startTime
     * @param endTime
     * @return
     */
    public Double spreadData(@Param("tableName") String tableName,
                             @Param("fieldName") String fieldName,
                             @Param("shipCode") String shipCode,
                             @Param("uniqueIndex") String uniqueIndex,
                             @Param("startTime") String startTime,
                             @Param("endTime") String endTime);


    /**
     * 查询最新的某一列数据
     *
     * @param tableName
     * @param fieldName
     * @param shipCode
     * @param uniqueIndex
     * @return
     */
    Double selectLastColumnData(@Param("tableName") String tableName,
                                @Param("fieldName") String fieldName,
                                @Param("shipCode") String shipCode,
                                @Param("uniqueIndex") String uniqueIndex);


    /**
     * 查询最新的某一行数据
     *
     * @param tableName
     * @param shipCode
     * @param uniqueIndex
     * @return
     */
    Map<String, Object> selectLastRowData(@Param("tableName") String tableName,
                                          @Param("shipCode") String shipCode,
                                          @Param("uniqueIndex") String uniqueIndex);


    /**
     * 查询某个时间的一行数据
     *
     * @param tableName
     * @param shipCode
     * @param uniqueIndex
     * @return
     */
    Map<String, Object> selectRowDataByTs(@Param("tableName") String tableName,
                                          @Param("shipCode") String shipCode,
                                          @Param("uniqueIndex") String uniqueIndex,
                                          @Param("ts") String ts);


    /**
     * 查询最新数据集合
     *
     * @param tableName 表名
     * @param shipCode 船舶code
     * @param uniqueIndex 设备序号
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    List<Map<String, Object>> selectRowDataList(@Param("tableName") String tableName,
                                                 @Param("shipCode") String shipCode,
                                                 @Param("uniqueIndex") String uniqueIndex,
                                                 @Param("startTime") String startTime,
                                                 @Param("endTime") String endTime);

    /**
     * 查询最新的某一列数据
     *
     * @param tableName
     * @param fieldName
     * @param shipCode
     * @return
     */
    Double selectLastColumnAvgData(@Param("tableName") String tableName,
                                   @Param("fieldName") String fieldName,
                                   @Param("shipCode") String shipCode);

    /**
     * 查询某一列平均值数据
     *
     * @param tableName
     * @param fieldName
     * @param shipCode
     * @return
     */
    Double selectColumnAvgData(@Param("tableName") String tableName,
                                   @Param("fieldName") String fieldName,
                                   @Param("shipCode") String shipCode,
                               @Param("uniqueIndex") String uniqueIndex,
                               @Param("startTime") String startTime,
                               @Param("endTime") String endTime);

    /**
     * 查询最新的某一列数据
     *
     * @param tableName
     * @param fieldName
     * @param shipCode
     * @return
     */
    Double selectLastColumnSumData(@Param("tableName") String tableName,
                                   @Param("fieldName") String fieldName,
                                   @Param("shipCode") String shipCode);





}
