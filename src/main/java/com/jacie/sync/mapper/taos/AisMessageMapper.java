package com.jacie.sync.mapper.taos;

import com.jacie.sync.domain.taos.AisLocation;
import com.jacie.sync.domain.taos.AisStaticInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lzg
 * @description ais消息
 * @date 2023/6/25 13:56
 */
public interface AisMessageMapper {

    /**
     * 新增消息1-3数据
     * @param message 数据
     * @return
     */
    int addDataOneToThree(AisLocation message);


    /**
     * 新增消息5数据
     * @param message 数据
     * @return
     */
    int addDataFive(AisStaticInfo message);

    /**
     * 查询最新消息1-3数据
     * @param mmsi MMSI号码
     * @return
     */
    AisLocation selectLastData(@Param("mmsi") String mmsi);

    /**
     * 查询最新消息5数据
     * @param mmsi MMSI号码
     * @return
     */
    AisStaticInfo selectLastDataFive(@Param("mmsi")String mmsi);


    /**
     * 航行轨迹
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param mmsi MMSI
     * @return
     */
    List<Map<String,Object>> trajectoryList(@Param("startTime") String startTime,
                               @Param("endTime") String endTime,
                               @Param("mmsi") String mmsi);

}
