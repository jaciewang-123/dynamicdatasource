package com.jacie.sync.mapper.taos;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.taos.Boiler;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lzg
 * @description 锅炉
 * @date 2023/4/11 15:45
 */
public interface BoilerMapper extends BaseMapper<Boiler> {

    /**
     * 查询最新一条数据
     * @param shipCode
     * @param uniqueIndex
     */
    public List<Boiler> selectNewData(@Param("shipCode") String shipCode, @Param("uniqueIndex") String uniqueIndex);

    /**
     * 查询累计油耗
     * @param shipCode
     * @param uniqueIndex
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    public List<Boiler> selectSumFlow(@Param("shipCode") String shipCode,
                                      @Param("uniqueIndex") String uniqueIndex,
                                      @Param("startTime") String startTime,
                                      @Param("endTime") String endTime);
}
