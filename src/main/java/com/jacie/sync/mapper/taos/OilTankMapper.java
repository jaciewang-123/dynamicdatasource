package com.jacie.sync.mapper.taos;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.taos.OilTank;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lzg
 * @description 油舱
 * @date 2023/5/9 15:50
 */
public interface OilTankMapper extends BaseMapper<OilTank> {

    /**
     * 查询最新一条数据
     * @param shipCode
     * @param uniqueIndex
     */
    public List<OilTank> selectNewData(@Param("shipCode") String shipCode, @Param("uniqueIndex") String uniqueIndex);


    public OilTank selectSumOilData(@Param("shipCode") String shipCode,
                                    @Param("uniqueIndex") String uniqueIndex,
                                    @Param("startTime") String startTime,
                                    @Param("endTime") String endTime);
}
