package com.jacie.sync.mapper.taos;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.taos.Panel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lzg
 * @description 导航数据
 * @date 2023/4/11 15:45
 */
public interface PanelMapper extends BaseMapper<Panel> {

    /**
     * 查询最新一条数据
     * @param shipCode
     * @param uniqueIndex
     */
    public List<Panel> selectNewData(@Param("shipCode") String shipCode, @Param("uniqueIndex") String uniqueIndex);

    /**
     * 计算差值(最大和最小值)
     * @param shipCode
     * @param uniqueIndex
     * @param startTime
     * @param endTime
     * @return
     */
    public Panel spreadPanelData(@Param("shipCode") String shipCode,
                             @Param("uniqueIndex") String uniqueIndex,
                             @Param("startTime") String startTime,
                             @Param("endTime") String endTime);


    /**
     * 船舶航行轨迹
     * @param shipCode 船舶编号
     * @param uniqueIndex 设备序号
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    List<Map<String,Object>> trajectorys(@Param("shipCode") String shipCode,
                                         @Param("uniqueIndex") String uniqueIndex,
                                         @Param("startTime") String startTime,
                                         @Param("endTime") String endTime);


    /**
     * 船舶时间范围内最高航速
     * @param shipCode 船舶编号
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    Double maxShipSpeed(@Param("shipCode") String shipCode,
                        @Param("startTime") String startTime,
                        @Param("endTime") String endTime);

    /**
     * 船舶时间范围内最低航速
     * @param shipCode 船舶编号
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    Double minShipSpeed(@Param("shipCode") String shipCode,
                        @Param("startTime") String startTime,
                        @Param("endTime") String endTime);


}
