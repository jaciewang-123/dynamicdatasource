package com.jacie.sync.mapper.taos;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jacie.sync.domain.taos.ElectricityGe;

/**
 * @Name ElectricityGeMapper
 * @Desc
 * @Author yangxinxia
 * @Date 2023/5/8 14:58
 * @Version 1.0
 */
public interface ElectricityGeMapper extends BaseMapper<ElectricityGe> {
}
