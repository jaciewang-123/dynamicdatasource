package com.jacie.sync.mapper.pg;

import com.jacie.sync.domain.pg.TabMrvEuReport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author jacie.wang
* @description 针对表【tab_mrv_eu_report】的数据库操作Mapper
* @createDate 2023-10-16 11:24:59
* @Entity generator.domain.TabMrvEuReport
*/
public interface TabMrvEuReportMapper extends BaseMapper<TabMrvEuReport> {

}




