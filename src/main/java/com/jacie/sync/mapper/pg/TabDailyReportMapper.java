package com.jacie.sync.mapper.pg;

import com.jacie.sync.domain.pg.TabDailyReport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author jacie.wang
* @description 针对表【tab_daily_report(每日日报表)】的数据库操作Mapper
* @createDate 2023-10-16 11:24:59
* @Entity generator.domain.TabDailyReport
*/
public interface TabDailyReportMapper extends BaseMapper<TabDailyReport> {

}




