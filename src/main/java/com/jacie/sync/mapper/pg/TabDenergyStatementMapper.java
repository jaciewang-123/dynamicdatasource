package com.jacie.sync.mapper.pg;

import com.jacie.sync.domain.pg.TabDenergyStatement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author jacie.wang
* @description 针对表【tab_denergy_statement(能耗能效与能耗指标表)】的数据库操作Mapper
* @createDate 2023-10-16 11:24:59
* @Entity generator.domain.TabDenergyStatement
*/
public interface TabDenergyStatementMapper extends BaseMapper<TabDenergyStatement> {

}




