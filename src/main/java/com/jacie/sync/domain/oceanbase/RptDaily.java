package com.jacie.sync.domain.oceanbase;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 每日报告对象 rpt_daily
 * 
 * @author jacie
 * @date 2023-09-21
 */
@Data
public class RptDaily
{
    private static final long serialVersionUID = 1L;

    /** 报告id */
    @ExcelIgnore
    private Double rptDailyId;

    /** 船舶id */
    @ExcelIgnore
    private Long shipId;

    /** 报告日期 */
    @ExcelProperty(value = {"日期"})
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(value = "yyyy-MM-dd")
    private Date rptDate;

    /** 艏吃水 */
    @ExcelProperty(value = {"装载","艏吃水(M)"})
    private Double foreDraft;

    //艉吃水
    @ExcelProperty(value = {"装载","艉吃水(M)"})
    private Double aftDraft;
    //载货量
    @ExcelProperty(value = {"装载","载货量(t)"})
    private Double cargo;
    //经度
    @ExcelIgnore
    private Double lat;
    //纬度
    @ExcelIgnore
    private Double lon;

    // 经度方向：东经or西经
    @ExcelProperty(value = {"船位","经度"})
    private String longitudeEw;
    // 纬度方向：南纬or北纬
    @ExcelProperty(value = {"船位","纬度"})
    private String latitudeNs;

    @ExcelIgnore
    private Double course;
    //航向
    @ExcelProperty(value = {"船位","航向"})
    @TableField(exist = false)
    private String strCourse;

    //舵角
    @ExcelProperty(value = {"船位","舵角"})
    private Double rudderAngle;
    // 航行距离
    @ExcelProperty(value = {"距离","日航向(nm)"})
    private Double distance;
    //对地速度
    @ExcelProperty(value = {"航速","对地(kn)"})
    private Double speedToGround;
    //对水速度
    @ExcelProperty(value = {"航速","对水(kn)"})
    private Double speedToWater;

    //对地滑失率
    @ExcelIgnore
    private Double slipRate;

    //航行时间
    @ExcelProperty(value = {"航速","航行时间(h)"})
    private Double sailingTime;
    //主机转速
    @ExcelProperty(value = {"主机","转速(rpm)"})
    private Double meRpm;
    //主机功率
    @ExcelProperty(value = {"主机","功率(kw)"})
    private Double mePowerRate;
    //真风速
    @ExcelProperty(value = {"受风","真风速(kn)"})
    private Double actualWindSpeed;
    //真风向
    @ExcelProperty(value = {"受风","真风向"})
    private Double actualWindDirection;
    //顺风/逆风
    @ExcelProperty(value = {"受风","顺风/逆风"})
    private Integer alongOrInverseToWind;
    // 航速高
    @ExcelIgnore
    private Double highSpeed;
    // 航速低
    @ExcelIgnore
    private Double lowSpeed;

    /** 燃油消耗量 */
    @ExcelProperty(value = {"燃油消耗量"})
    private String consumeFuels;

    /** 主机燃油消耗量 */
    @ExcelProperty(value = "主机燃油消耗量")
    private String meFuels;

    /** 辅机燃油消耗量 */
    @ExcelProperty(value = "辅机燃油消耗量")
    private String auxFuels;

    /** 锅炉燃油消耗量 */
    @ExcelProperty(value = "锅炉燃油消耗量")
    private String boilerFuels;

    /** 加油量 */
    @ExcelProperty(value = "加油量")
    private String addFuels;

    /** 燃油存量 */
    @ExcelProperty(value = "燃油存量")
    private String storageFuels;

    /**
     * 创建时间
     */
    @ExcelIgnore
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
