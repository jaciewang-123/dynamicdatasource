package com.jacie.sync.domain.oceanbase;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 能效指标数据报告对象 rpt_energy_index
 * 
 * @author jacie
 * @date 2023-09-21
 */
@Data
public class RptEnergyIndex
{
    private static final long serialVersionUID = 1L;

    /** 报告ID */
    @ExcelIgnore
    private Long id;

    /** 船舶ID */
    @ExcelIgnore
    private Long shipId;

    /** 报告日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ExcelProperty(value = "报告日期")
    @DateTimeFormat(value = "yyyy-MM-dd")
    private Date reportDate;

    /** 报告日期字符串 */
    @ExcelIgnore
    private String reportDateStr;

    /** 平均载货(t) */
    @ExcelProperty(value = "平均载货(t)")
    private Double avgCargo;

    /** 航行距离(nm) */
    @ExcelProperty(value = "航行距离(nm)")
    private Double distance;

    /** 总油耗(t) */
    @ExcelProperty(value = "总油耗(t)")
    private Double totalOilConsume;

    /** 总CO2排放 */
    @ExcelProperty(value = "总CO2排放")
    private Double totalCo2Emission;

    /** 总运输功(t*nm) */
    @ExcelProperty(value = "总运输功(t*nm)")
    private Double totalTransportWork;

    /** 单位距离燃油消耗(t/nm) */
    @ExcelProperty(value = "单位距离燃油消耗(t/nm)")
    private Double fuelConsumePerDistance;

    /** 单位运输功燃油消耗(g/(t*nm)) */
    @ExcelProperty(value = "单位运输功燃油消耗(g/(t*nm))")
    private Double fuelConsumePerTransportWork;

    /** 单位距离CO2排放(kgCO2/(nm)) */
    @ExcelProperty(value = "单位距离CO2排放(kgCO2/(nm))")
    private Double co2EmissionPerDistance;

    /** 单位运输量CO2排放(g/(t*nm)) */
    @ExcelProperty(value = "单位运输量CO2排放(g/(t*nm))")
    private Double co2EmissionPerTransportWork;

    /** 单位运输功CO2排放(gCO2/(t*nm)) */
    @ExcelProperty(value = "单位运输功CO2排放(gCO2/(t*nm))")
    private Double co2EmissionPerTransportPower;

    /** EEIO(g/t*nm) */
    @ExcelProperty(value = "EEIO(g/t*nm)")
    private Double eeio;

    /** 数据类型(1-每天 3-每月 4-每年 5-每航次) */
    @ExcelIgnore
    private Integer dataType;

    /**
     * 创建时间
     */
    @ExcelIgnore
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;



    /**
     * 开始时间
     */
    @ExcelIgnore
    private String routeStartTime;

    /**
     * 结束时间
     */
    @ExcelIgnore
    private String routeEndTime;
}
