package com.jacie.sync.domain.oceanbase;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 每天油舱余量记录
 * @TableName snpt_tank_daily
 */
@TableName(value ="snpt_tank_daily")
@Data
public class SnptTankDaily implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 船舶编号
     */
    private Long shipId;

    /**
     * 油舱液位(立方米)
     */
    private Double tank1;

    /**
     * 油舱液位(立方米)
     */
    private Double tank2;

    /**
     * 油舱液位(立方米)
     */
    private Double tank3;

    /**
     * 油舱液位(立方米)
     */
    private Double tank4;

    /**
     * 油舱液位(立方米)
     */
    private Double tank5;

    /**
     * 油舱液位(立方米)
     */
    private Double tank6;

    /**
     * 油舱液位(立方米)
     */
    private Double tank7;

    /**
     * 油舱液位(立方米)
     */
    private Double tank8;

    /**
     * 油舱液位(立方米)
     */
    private Double tank9;

    /**
     * 油舱液位(立方米)
     */
    private Double tank10;

    /**
     * 油舱液位(立方米)
     */
    private Double tank11;

    /**
     * 油舱液位(立方米)
     */
    private Double tank12;

    /**
     * 油舱液位(立方米)
     */
    private Double tank13;

    /**
     * 油舱液位(立方米)
     */
    private Double tank14;

    /**
     * 油舱液位(立方米)
     */
    private Double tank15;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 数据时间
     */
    private String dataTime;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt1;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt2;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt3;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt4;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt5;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt6;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt7;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt8;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt9;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt10;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt11;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt12;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt13;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt14;

    /**
     * 油舱流量(kg/h)
     */
    private Double flowAmt15;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}