package com.jacie.sync.domain.oceanbase;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * MRV报告
 * @TableName snpt_mrv
 */
@TableName(value ="snpt_mrv")
@Data
public class SnptMrv implements Serializable {
    /**
     * 报告id
     */
    @TableId
    private Long reportId;

    /**
     * 报告日期
     */
    private String reportDate;

    private Date dataTime;

    /**
     * 船舶id
     */
    private Long shipId;

    /**
     * MMSI
     */
    private String mmsi;

    /**
     * 呼号
     */
    private String callSign;

    /**
     * 船舶种类
     */
    private String shipCategory;

    /**
     * 总载重吨
     */
    private Double deadweightTon;


    /**
     * 航线id
     */
    private String routeId;

    /**
     * 航线名称
     */
    private String routeName;

    /**
     * 航次id
     */
    private Long voyageId;

    /**
     * 航次号
     */
    private String voyageNo;


    /**
     * 燃油消耗(包含种类和消耗量)
     */
    private String fuelsConsume;

    /**
     * 总油耗
     */
    @TableField(exist = false)
    private Double totalConsume;

    /**
     * co2排放量
     */
    private Double co2Emissions;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 报告类型 1-每天 2-每周 3-每月 4-每年 5-每航次
     */
    private Integer reportType;

    @TableField(exist = false)
    private String shipName;

    @TableField(exist = false)
    private String shipEnName;
}