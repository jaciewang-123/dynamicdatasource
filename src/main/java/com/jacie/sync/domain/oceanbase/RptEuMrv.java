package com.jacie.sync.domain.oceanbase;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * MRV-EU对象 rpt_eu_mrv
 * 
 * @author jacie
 * @date 2023-09-21
 */
public class RptEuMrv
{
    private static final long serialVersionUID = 1L;

    /** 报告ID */
    @ExcelIgnore
    private Long rptEuId;

    /** 船舶id */
    @ExcelIgnore
    private Long shipId;

    /** 航次编号 */
    @ExcelProperty(value = {"航次编号"})
    private String voyageNo;

    /** 航次ID */
    @ExcelIgnore
    private Long voyageId;

    /** 出发时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ExcelProperty(value = {"港口信息","出发时间"})
    @DateTimeFormat(value = "yyyy-MM-dd")
    private Date departTime;

    /** 出发港口 */
    
    @ExcelProperty(value = {"港口信息","出发港口"})
    private String departPort;

    /** 出发港口id */
    @ExcelIgnore
    private String departPortId;

    /** 出发港是否欧盟 */
    @ExcelIgnore
    private Integer departPortEu;

    @ExcelProperty(value = {"港口信息","出发港是否欧盟"})
    @TableField(exist = false)
    private String isDepartPortEu;

    /** 到达时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ExcelProperty(value = {"港口信息","到达时间"})
    @DateTimeFormat(value = "yyyy-MM-dd")
    private Date arrivalTime;

    /** 目的港口 */
    @ExcelProperty(value = {"港口信息","目的港口"})
    private String arrivalPort;

    /** 目的港口id */
    @ExcelIgnore
    private String arrivalPortId;

    /** 目的港是否欧盟 */
    @ExcelIgnore
    private Integer arrivalPortEu;

    @ExcelProperty(value = {"港口信息","目的港是否欧盟"})
    @TableField(exist = false)
    private String isArrivalPortEu;

    /** 载货量(t) */
    @ExcelProperty(value = {"综合信息","载货量(t)"})
    private Double cargoCapacity;

    /** 航行时间 */
    @ExcelProperty(value = {"综合信息","航行时间"})
    private Double sailingTime;

    /** 航行距离 */
    @ExcelProperty(value = {"综合信息","航行距离"})
    private Double distance;

    /** 航行途中燃油消耗量 */
    @ExcelProperty(value = {"航行途中燃油消耗量"})
    private String fuelConsumeVoyage;

    /** 港内消耗量 */
    @ExcelProperty(value = {"港内消耗量"})
    private String fuelConsumePort;

    /**
     * 创建时间
     */
    @ExcelIgnore
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    public String getIsDepartPortEu() {
        return isDepartPortEu;
    }

    public void setIsDepartPortEu(String isDepartPortEu) {
        this.isDepartPortEu = isDepartPortEu;
    }

    public String getIsArrivalPortEu() {
        return isArrivalPortEu;
    }

    public void setIsArrivalPortEu(String isArrivalPortEu) {
        this.isArrivalPortEu = isArrivalPortEu;
    }

    public Long getRptEuId() {
        return rptEuId;
    }

    public void setRptEuId(Long rptEuId) {
        this.rptEuId = rptEuId;
    }

    public Long getShipId() {
        return shipId;
    }

    public void setShipId(Long shipId) {
        this.shipId = shipId;
    }

    public String getVoyageNo() {
        return voyageNo;
    }

    public void setVoyageNo(String voyageNo) {
        this.voyageNo = voyageNo;
    }

    public Long getVoyageId() {
        return voyageId;
    }

    public void setVoyageId(Long voyageId) {
        this.voyageId = voyageId;
    }

    public Date getDepartTime() {
        return departTime;
    }

    public void setDepartTime(Date departTime) {
        this.departTime = departTime;
    }

    public String getDepartPort() {
        return departPort;
    }

    public void setDepartPort(String departPort) {
        this.departPort = departPort;
    }

    public String getDepartPortId() {
        return departPortId;
    }

    public void setDepartPortId(String departPortId) {
        this.departPortId = departPortId;
    }

    public Integer getDepartPortEu() {
        return departPortEu;
    }

    public void setDepartPortEu(Integer departPortEu) {
        this.departPortEu = departPortEu;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getArrivalPort() {
        return arrivalPort;
    }

    public void setArrivalPort(String arrivalPort) {
        this.arrivalPort = arrivalPort;
    }

    public String getArrivalPortId() {
        return arrivalPortId;
    }

    public void setArrivalPortId(String arrivalPortId) {
        this.arrivalPortId = arrivalPortId;
    }

    public Integer getArrivalPortEu() {
        return arrivalPortEu;
    }

    public void setArrivalPortEu(Integer arrivalPortEu) {
        this.arrivalPortEu = arrivalPortEu;
    }

    public Double getCargoCapacity() {
        return cargoCapacity;
    }

    public void setCargoCapacity(Double cargoCapacity) {
        this.cargoCapacity = cargoCapacity;
    }

    public Double getSailingTime() {
        return sailingTime;
    }

    public void setSailingTime(Double sailingTime) {
        this.sailingTime = sailingTime;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getFuelConsumeVoyage() {
        return fuelConsumeVoyage;
    }

    public void setFuelConsumeVoyage(String fuelConsumeVoyage) {
        this.fuelConsumeVoyage = fuelConsumeVoyage;
    }

    public String getFuelConsumePort() {
        return fuelConsumePort;
    }

    public void setFuelConsumePort(String fuelConsumePort) {
        this.fuelConsumePort = fuelConsumePort;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    @Override
    public String toString() {
        return "RptEuMrv{" +
                "rptEuId=" + rptEuId +
                ", shipId=" + shipId +
                ", voyageNo='" + voyageNo + '\'' +
                ", voyageId=" + voyageId +
                ", departTime=" + departTime +
                ", departPort='" + departPort + '\'' +
                ", departPortId='" + departPortId + '\'' +
                ", departPortEu=" + departPortEu +
                ", arrivalTime=" + arrivalTime +
                ", arrivalPort='" + arrivalPort + '\'' +
                ", arrivalPortId='" + arrivalPortId + '\'' +
                ", arrivalPortEu=" + arrivalPortEu +
                ", cargoCapacity=" + cargoCapacity +
                ", sailingTime=" + sailingTime +
                ", distance=" + distance +
                ", fuelConsumeVoyage='" + fuelConsumeVoyage + '\'' +
                ", fuelConsumePort='" + fuelConsumePort + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
