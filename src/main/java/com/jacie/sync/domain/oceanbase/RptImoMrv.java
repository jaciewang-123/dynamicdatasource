package com.jacie.sync.domain.oceanbase;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;


/**
 * MRV-IMO对象 rpt_imo_mrv
 * 
 * @author jacie
 * @date 2023-09-21
 */
@Data
public class RptImoMrv
{
    private static final long serialVersionUID = 1L;

    /** 报告ID */
    @ExcelIgnore
    private Long rptImoId;

    /** 船舶id */
    @ExcelIgnore
    private Long shipId;

    /** 报告日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ExcelProperty(value = "报告日期")
    @DateTimeFormat(value = "yyyy-MM-dd")
    private Date rptDate;

    /** 报告日期字符串 */
    @ExcelIgnore
    private String rptDateStr;

    /** 航行时间 */
    @ExcelProperty(value = {"综合信息","航行时间"})
    private Double sailingTime;

    /** 航行距离 */
    @ExcelProperty(value = {"综合信息","航行距离"})
    private Double distanceTraveled;

    /** 燃油消耗量 */
    @ExcelProperty(value = {"燃油消耗量"})
    private String fuelConsumes;

    /** 燃油剩余量 */
    @ExcelProperty(value = {"燃油剩余量"})
    private String fuelRemains;

    /** 加油量 */
    @ExcelProperty(value = {"加油量"})
    private String refuelingAmounts;

    /** 数据类型(1-每天 3-每月 4-每年 5-每航次) */
    @ExcelIgnore
    private Integer dataType;

    /**
     * 创建时间
     */
    @ExcelIgnore
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 开始时间
     */
    @ExcelIgnore
    private String flightStartTime;

    /**
     * 结束时间
     */
    @ExcelIgnore
    private String flightEndTime;

}
