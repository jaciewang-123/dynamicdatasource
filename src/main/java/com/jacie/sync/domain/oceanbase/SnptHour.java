package com.jacie.sync.domain.oceanbase;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 每小时统计计算值表
 * @TableName snpt_hour
 */
@TableName(value ="snpt_hour")
@Data
public class SnptHour implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 船舶id
     */
    private Long shipId;

    /**
     * 主机油耗(kg)
     */
    private Double meOilConsum;

    /**
     * 辅机(发电机)油耗(kg)
     */
    private Double dgOilConsum;

    /**
     * 锅炉油耗(kg)
     */
    private Double boilerOilConsum;

    /**
     * 航行距离(nm)
     */
    private Double distance;

    /**
     * 航行时间(分钟)
     */
    private Double voyageTime;

    /**
     * 总油耗
     */
    private Double totalOilConsum;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /**
     * 每海里油耗(油耗/距离 或者 瞬时油耗/航速)
     */
    private Double oilMileConsum;

    /**
     * 每运输功燃油消耗量
     */
    private Double oilTransWorkConsum;

    /**
     * 数据时间
     */
    private String dataTime;

    /**
     * 滑失率
     */
    private Double slipRate;
    /**
     * 轻转裕度
     */
    private Double lightTurning;

    /**
     * 碳排放
     */
    private Double carbonEmission;
    /**
     * 氮排放
     */
    private Double nitrogenEmission;
    /**
     * 硫排放
     */
    private Double sulfurEmission;

    /**
     * 航行状态
     */
    private String navigationalStatus;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}