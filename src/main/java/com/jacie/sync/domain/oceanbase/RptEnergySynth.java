package com.jacie.sync.domain.oceanbase;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 能效能耗综合报告对象 rpt_energy_synth
 * 
 * @author jacie
 * @date 2023-09-21
 */
@Data
public class RptEnergySynth
{
    private static final long serialVersionUID = 1L;

    /** 报告ID */
    @ExcelIgnore
    private Long id;

    /** 船舶ID */
    @ExcelIgnore
    private Long shipId;

    /** 报告日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ExcelProperty(value = "报告日期")
    @DateTimeFormat(value = "yyyy-MM-dd")
    private Date reportDate;

    /** 报告日期字符串 */
    @ExcelIgnore
    private String reportDateString;

    /** 航行时间(h) */
    @ExcelProperty(value = {"综合信息","航行时间(h)"})
    private Double sailingTime;

    /** 平均载货(t) */
    @ExcelProperty(value = {"综合信息","平均载货(t)"})
    private Double averageCargo;

    /** 航行距离(nm) */
    @ExcelProperty(value = {"综合信息","航行距离(nm)"})
    private Double navigationalDistance;

    /** 总油耗(t) */
    @ExcelProperty(value = {"综合信息","总油耗(t)"})
    private Double totalFuel;

    /** 总CO2排放 */
    @ExcelProperty(value = {"综合信息","总CO2排放"})
    private Double totalCo2;

    /** 总运输功(t*nm) */
    @ExcelProperty(value = {"综合信息","总运输功(t*nm)"})
    private Double totalTransportWork;

    /** 平均航速(kn) */
    @ExcelProperty(value = {"综合信息","平均航速(kn)"})
    private Double averageSpeed;

    /** 主机航行油耗(t) */
    @ExcelProperty(value = {"主机航行","油耗(t)"})
    private Double hostSailOil;

    /** 主机航行油耗率(t/day) */
    @ExcelProperty(value = {"主机航行","油耗率(t/day)"})
    private Double hostSailOilRate;

    /** 主机航行超限比例(%) */
    @ExcelProperty(value = {"主机航行","超限比例(%)"})
    private Double hostSailProportion;

    /** 辅机航行油耗(t) */
    @ExcelProperty(value = {"辅机航行","油耗(t)"})
    private Double auxSailOil;

    /** 辅机航行油耗率(t/day) */
    @ExcelProperty(value = {"辅机航行","油耗率(t/day)"})
    private Double auxSailOilRate;

    /** 辅机航行超限比例(%) */
    @ExcelProperty(value = {"辅机航行","超限比例(%)"})
    private Double auxSailProportion;

    /** 辅机停泊油耗(t) */
    @ExcelProperty(value = {"辅机停泊","油耗(t)"})
    private Double auxStopOil;

    /** 辅机停泊油耗率(t/day) */
    @ExcelProperty(value = {"辅机停泊","油耗率(t/day)"})
    private Double auxStopOilRate;

    /** 辅机停泊超限比例(%) */
    @ExcelProperty(value = {"辅机停泊","超限比例(%)"})
    private Double auxStopProportion;

    /** 锅炉航行油耗(t) */
    @ExcelProperty(value = {"锅炉航行","油耗(t)"})
    private Double fuelSailOil;

    /** 锅炉航行油耗率(t/day) */
    @ExcelProperty(value = {"锅炉航行","油耗率(t/day)"})
    private Double fuelSailOilRate;

    /** 锅炉航行超限比例(%) */
    @ExcelProperty(value = {"锅炉航行","超限比例(%)"})
    private Double fuelSailProportion;

    /** 锅炉停泊油耗(t) */
    @ExcelProperty(value = {"锅炉停泊","油耗(t)"})
    private Double fuelStopOil;

    /** 锅炉停泊油耗率(t/day) */
    @ExcelProperty(value = {"锅炉停泊","油耗率(t/day)"})
    private Double fuelStopOilRate;

    /** 锅炉停泊超限比例(%) */
    @ExcelProperty(value = {"锅炉停泊","超限比例(%)"})
    private Double fuelStopProportion;

    /** 单位距离燃料消耗(t/nm) */
    @ExcelProperty(value = {"单位距离燃料消耗","计算值(t/nm)"})
    private Double unitDistanceOil;

    /** 单位距离燃料消耗超限比例(%) */
    @ExcelProperty(value = {"单位距离燃料消耗","超限比例(%)"})
    private Double unitDistanceOilProportion;

    /** 单位运输功燃料消耗(g/(t*nm)) */
    @ExcelProperty(value = {"单位运输功燃料消耗","计算值(g/(t*nm))"})
    private Double unitWorkOil;

    /** 单位运输功燃料消耗超限比例(%) */
    @ExcelProperty(value = {"单位运输功燃料消耗","超限比例(%)"})
    private Double unitWorkOilProportion;

    /** 单位距离CO2排放(t/nm) */
    @ExcelProperty(value = {"单位距离CO2排放","计算值(t/nm)"})
    private Double unitDistanceCo2;

    /** 单位距离CO2排放超限比例(%) */
    @ExcelProperty(value = {"单位距离CO2排放","超限比例(%)"})
    private Double unitDistanceCo2Proportion;

    /** 单位运输功CO2排放(gCO₂/(t*nm)) */
    @ExcelProperty(value = {"单位运输功CO2排放","计算值(gCO₂/(t*nm))"})
    private Double unitWorkCo2;

    /** 单位运输功CO2排放超限比例(%) */
    @ExcelProperty(value = {"单位运输功CO2排放","超限比例(%)"})
    private Double unitWorkCo2Proportion;

    /** 单位运输量CO2排放(t/nm) */
    @ExcelProperty(value = {"单位运输量CO2排放","计算值(t/nm)"})
    private Double unitTransportationCo2;

    /** 单位运输量CO2排放超限比例(%) */
    @ExcelProperty(value = {"单位运输量CO2排放","超限比例(%)"})
    private Double unitTransportationCo2Proportion;

    /** 单位距离SO2排放 */
    @ExcelProperty(value = {"单位距离SO2排放","计算值"})
    private Double unitDistanceSo2;

    /** 单位距离SO2排放超限比例 */
    @ExcelProperty(value = {"单位距离SO2排放","超限比例"})
    private Double unitDistanceSo2proportion;

    /** 单位运输功SO2排放 */
    @ExcelProperty(value = {"单位运输功SO2排放","计算值"})
    private Double unitWorkSo2;

    /** 单位运输功SO2排放超限比例 */
    @ExcelProperty(value = {"单位运输功SO2排放","超限比例"})
    private Double unitWorkSo2Proportion;

    /** EEOL能量指标数据报告 */
    @ExcelIgnore
    private String eeoi;

    /** 综合评估,是否超限 */
    @ExcelIgnore
    private Integer allAssessment;

    @ExcelProperty(value = {"综合评估","是否超限"})
    @TableField(exist = false)
    private String isAssessment;

    /**
     * 数据类型(1-每天 3-每月 4-每年 5-每航次)
     */
    @ExcelIgnore
    private Integer dataType;

    /**
     * 创建时间
     */
    @ExcelIgnore
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 开始时间
     */
    @ExcelIgnore
    private String routeStartTime;

    /**
     * 结束时间
     */
    @ExcelIgnore
    private String routeEndTime;


}
